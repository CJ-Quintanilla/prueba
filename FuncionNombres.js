function ordenarNombresYContar(nombres, ascendente = true) {
  const nombresCopiados = [...nombres];

  nombresCopiados.sort((a, b) => (ascendente ? a.localeCompare(b) : b.localeCompare(a)));

  const contador = {};
  const nombresOrdenados = nombresCopiados.map(nombre => {
    contador[nombre] = (contador[nombre] || 0) + 1;
    return nombre;
  });

  const nombresAE = nombresCopiados.filter(nombre => nombre.startsWith('A') || nombre.startsWith('E')).length;

  const repeticiones = Object.values(contador).reduce((total, count) => total + (count > 1), 0);

  return {
    nombresOrdenados,
    repeticiones,
    nombresAE
  };
}

const nombres = ['Juan', 'Ana', 'Pedro', 'Maria', 'Luis', 'Elena', 'Ana', 'Esteban', 'Elena'];
const resultado = ordenarNombresYContar(nombres);
console.log("Nombres ordenados:", resultado.nombresOrdenados);
console.log("Nombres repetidos:", resultado.repeticiones);
console.log("Nombres que comienzan con 'A' o 'E':", resultado.nombresAE);
